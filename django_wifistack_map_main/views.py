import requests
from django.conf import settings
from django.http import HttpResponse
from django.template import loader

from django_wifistack_map_main.django_wifistack_map_main.models import *


def get_meta_data():
    meta_data = requests.get(settings.CDSTACK_META_API_URL)

    return meta_data.json()


def map(request):
    current_site = Site.objects.get_current(request)
    current_site_config = WifiMapSiteConf.objects.get(site=current_site)

    template_opts = dict(get_meta_data())
    template_opts["node_map_title"] = current_site_config.main_title
    template_opts["intro_popup_html"] = current_site_config.intro_popup_html

    template_opts["data"] = settings.WIFISTACK_GEOJSON_URL

    template = loader.get_template("django_wifistack_map_main/map.html")

    return HttpResponse(template.render(template_opts, request))


def static_data_privacy(request):
    template_opts = dict(get_meta_data())

    template = loader.get_template("django_wifistack_map_main/data_privacy.html")

    return HttpResponse(template.render(template_opts, request))


def static_disclaimer(request):
    template_opts = dict(get_meta_data())

    template = loader.get_template("django_wifistack_map_main/disclaimer.html")

    return HttpResponse(template.render(template_opts, request))


def static_imprint(request):
    template_opts = dict(get_meta_data())

    template = loader.get_template("django_wifistack_map_main/imprint.html")

    return HttpResponse(template.render(template_opts, request))
