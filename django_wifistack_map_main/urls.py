from django.urls import path

from . import views

urlpatterns = [
    path("", views.map),
    path("data-privacy/", views.static_data_privacy, name="data-privacy"),
    path("disclaimer/", views.static_disclaimer, name="disclaimer"),
    path("imprint/", views.static_imprint, name="imprint"),
]
