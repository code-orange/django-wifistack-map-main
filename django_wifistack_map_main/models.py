from django.contrib.sites.models import Site
from django.db import models


class WifiMapSiteConf(models.Model):
    main_title = models.CharField(max_length=200, default="Wireless Nodemap")
    site = models.OneToOneField(Site, models.CASCADE)
    intro_popup_html = models.TextField(default="Welcome to our Wireless Nodemap!")

    class Meta:
        db_table = "wifi_map_site_conf"
