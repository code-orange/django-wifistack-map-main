# Generated by Django 2.2.3 on 2019-07-04 23:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_wifistack_map_main", "0001_add_site_specific_config"),
    ]

    operations = [
        migrations.AddField(
            model_name="wifimapsiteconf",
            name="intro_popup_html",
            field=models.TextField(default="Welcome to our Wireless Nodemap!"),
        ),
    ]
